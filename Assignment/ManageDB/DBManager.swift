//
//  DBManager.swift

import UIKit
import CoreData
class DBManager: NSObject {
    var saveData_object = [DownloadDB]()
    static let sharedInstance = DBManager()
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    var managedContext = NSManagedObjectContext()
    
    override init(){
        super.init()
        if #available(iOS 10.0, *) {
            self.managedContext = CoreDataModel.sharedManager.persistentContainer.viewContext
        } else {
            self.managedContext = CoreDataModel.sharedManager.managedObjectContext
        }
    }
    
    //** Save Download Data name and uuid **//
    func saveDownloadData (_ Description: String, _ Contentype: String, _ createdby:Date,_ file:Data,_ filename:String,_ url:String ) {
        let entity = NSEntityDescription.entity(forEntityName: "DownloadDB",  in: managedContext)!
        let object = DownloadDB (entity: entity,  insertInto: managedContext)
        object.desc = Description
        object.contentype = Contentype
        object.file = file
        object.createdby = createdby
        object.filename = filename
        object.url = url
        do {
            try managedContext.save()
        } catch { //let error as NSError
        }
    }
    
    func getSaveData() -> [DownloadDB] {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "DownloadDB")
        do {
            saveData_object = try managedContext.fetch(fetchRequest) as! [DownloadDB]
        } catch { //let error as NSError
        }
        return saveData_object
    }
    
    func upDateSaveData(saveData:DownloadDB,description:String) {
        saveData.desc = description
        do {
            try managedContext.save()
            //            print("saved!")
        } catch  { //let error as NSError
            
        }
    }
}

