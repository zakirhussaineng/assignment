//
//  ImageVC.swift
//  Assignment

import UIKit

class ImageVC: UIViewController {
    @IBOutlet weak var ImageView:UIImageView!
    var strData:Data?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ImageView.image = UIImage(data:strData!)
        // Do any additional setup after loading the view.
    }
}
