//
//  ViewVC.swift
//  Assignment

import UIKit
import AVFoundation

class ViewVC: UIViewController {
    var SaveDownloadData = [DownloadDB]()
    @IBOutlet weak var tblViewList:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SaveDownloadData = DBManager.sharedInstance.getSaveData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.SaveDownloadData.count == 0 {
            let alert = UIAlertController(title: "Alert", message:"Data not found.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
extension ViewVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SaveDownloadData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"DTableViewCell") as! DTableViewCell
        cell.btnView.addTarget(self, action: #selector(btn1Click( sender:)), for: .touchUpInside)
        cell.lblFileformat.text = self.SaveDownloadData[indexPath.row].contentype
        cell.lbldesc.text = self.SaveDownloadData[indexPath.row].desc
        cell.btnView.tag = indexPath.row
        cell.lblfileName.text =  self.SaveDownloadData[indexPath.row].filename
        return cell
    }
    
    @objc func btn1Click(sender: UIButton) {
        if self.SaveDownloadData[sender.tag].contentype!.contains("Image")  {
            let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
            let vc = storyboard.instantiateViewController(identifier:"ImageVC") as! ImageVC
            vc.strData = self.SaveDownloadData[sender.tag].file
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (self.SaveDownloadData[sender.tag].contentype?.contains("MP3"))!{
            let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
            let vc = storyboard.instantiateViewController(identifier:"PDFVC") as! PDFVC
            vc.strTypes = "MP3"
            vc.ContentValue = self.SaveDownloadData[sender.tag].filename ?? ""
            vc.pthURL = URL(string: String(self.SaveDownloadData[sender.tag].url ?? ""))
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (self.SaveDownloadData[sender.tag].contentype?.contains("PDF"))!{
            let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
            let vc = storyboard.instantiateViewController(identifier:"PDFVC") as! PDFVC
            vc.strTypes = "PDF"
            vc.ContentValue = self.SaveDownloadData[sender.tag].filename ?? ""
            vc.pthURL = URL(string: String(self.SaveDownloadData[sender.tag].url ?? ""))
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (self.SaveDownloadData[sender.tag].contentype?.contains("MP4"))!{
            let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
            let vc = storyboard.instantiateViewController(identifier:"PDFVC") as! PDFVC
            vc.strTypes = "MP4"
            vc.ContentValue = self.SaveDownloadData[sender.tag].filename ?? ""
            vc.pthURL = URL(string: String(self.SaveDownloadData[sender.tag].url ?? ""))
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{}
    }
}
