//
//  PDFVC.swift
//  Assignment

import UIKit
import PDFKit
import AVKit
import AVFoundation

class PDFVC: UIViewController {
    
    @IBOutlet var pdfView: PDFView!
    @IBOutlet var btnPlay: UIButton!
    var playeritem:AVPlayerItem?
    var avplayerViewController = AVPlayerViewController()
    var player:AVPlayer?
    var strTypes = ""
    var ContentValue = ""
    var pthURL:URL!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.strTypes == "PDF"{
            let fileManager = FileManager.default
            let docsUrl = try! fileManager.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let component = self.ContentValue
            let fileUrl = docsUrl.appendingPathComponent(component)
            
            self.pdfView.isHidden = false
            self.btnPlay.isHidden = true
            if let document = PDFDocument(url:fileUrl) {
                pdfView.document = document
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                self.dismiss(animated: true, completion: nil)
            }
        }else if self.strTypes == "MP4"{
            let fileManager = FileManager.default
            let docsUrl = try! fileManager.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let component = self.ContentValue
            self.btnPlay.isHidden = false
            let fileUrl = docsUrl.appendingPathComponent(component)
            print("save file \(fileUrl)")
            self.player = AVPlayer(url:fileUrl)
            self.avplayerViewController.player = self.player
            self.pdfView.isHidden = true
        }
        else{
            let fileManager = FileManager.default
            let docsUrl = try! fileManager.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let component = self.ContentValue
            let fileUrl = docsUrl.appendingPathComponent(component)
            self.btnPlay.isHidden = false
            self.playeritem = AVPlayerItem(url: fileUrl)
            self.player = AVPlayer(playerItem: self.playeritem)
            let playerLayer=AVPlayerLayer(player: player!)
            playerLayer.frame = CGRect(x:50, y: 100, width: 200, height: 100)
            self.pdfView.isHidden = true
            self.view.layer.addSublayer(playerLayer)
        }
        // Do any additional setup after loading the view.
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    @IBAction func btnPlay(_ sender:UIButton)
    {
        if self.strTypes == "MP4"{
            self.present(self.avplayerViewController, animated: true) {
                self.avplayerViewController.player?.play()
            }
        }else{
            if player?.rate == 0{
                player!.play()
                btnPlay.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            } else {
                player!.pause()
                btnPlay.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            }
        }
    }
}
