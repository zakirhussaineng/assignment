//
//  DTableViewCell.swift
//  Assignment

import UIKit

class DTableViewCell: UITableViewCell {
    @IBOutlet weak var lbldesc: UILabel!
    @IBOutlet weak var lblFileformat: UILabel!
    @IBOutlet weak var lblfileName: UILabel!
    @IBOutlet var btnView: UIButton!
    @IBOutlet var NewimageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
