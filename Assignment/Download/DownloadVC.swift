//
//  DownloadVC.swift
//  Assignment

import UIKit
import Alamofire
import DropDown
import MobileCoreServices
import PKHUD
class DownloadVC: UIViewController {
    @IBOutlet weak var btnDownload:UIButton!
    @IBOutlet weak var btnDropdown:UIButton!
    @IBOutlet weak var btnContentType:UIButton!
    @IBOutlet weak var txtURL:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    @IBOutlet weak var progressView: UIProgressView!
    var dropdown = DropDown()
    var arrContentType =  ["Image","MP3","MP4","PDF","OTHER"]
    var ContentType = ""
    var pdfFileUrl = ""
    var pdfURL = ""
    var task : URLSessionTask!
    var AlertMessage = ""
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AlertMessage = "Url content type wrong."
        self.txtDescription.text = "Write your Description"
        self.txtDescription.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UIButton Action
    
    @IBAction func btnDownload(_ sender: UIButton) {
        
        if self.txtURL.text == "" {
            self.AlertControler(message:"Please enter url.")
        }
        else if (self.ContentType == ""){
            self.AlertControler(message:"Please select your content type.")
        }else if(self.txtDescription.text == "Write your Description"){
            self.AlertControler(message:"Please input your description.")
        }
        else if (self.txtDescription.text == ""){
            self.AlertControler(message:"Please input your description.")
        }
        else if !isValidUrl(url: txtURL.text ?? ""){
            self.AlertControler(message:"Please enter valid url.")
        }else{
            let strURL = URL.mimeType(URL(string:self.txtURL.text ?? "")!)()
            switch ContentType {
            case "PDF":
                if strURL == "application/pdf"{
                    self.CallDownloadFile(strURL:self.txtURL.text ?? "")
                }else{
                    self.AlertControler(message:self.AlertMessage)
                }
                break
            case "MP3":
                if strURL == "audio/mpeg"{
                    self.CallDownloadFile(strURL:self.txtURL.text ?? "")
                }else{
                    self.AlertControler(message:self.AlertMessage)
                }
                break
            case "Image":
                if (strURL == "image/jpeg") ||  (strURL == "image/png"){
                    self.CallDownloadUrl(Url:self.txtURL.text ?? "", Desc:self.txtDescription.text, contentType:self.ContentType)
                }else{
                    self.AlertControler(message:self.AlertMessage)
                }
                break
            case "MP4":
                if strURL == "video/mp4"{
                    self.CallDownloadFile(strURL:self.txtURL.text ?? "")
                }else{
                    self.AlertControler(message: self.AlertMessage)
                }
                break
            default:
                self.AlertControler(message:"Its Url doesn't exits.")
            }
        }
    }
    
    // MARK: - UIButton Action
    @IBAction func btnContentType(_ sender: UIButton) {
        self.dropdown.anchorView = sender
        self.dropdown.show()
        self.dropdown.dataSource = self.arrContentType
        self.dropdown.selectionAction = {(index:Int,item:String) in
            self.ContentType = item
            self.btnContentType.setTitle(item, for:.normal)
            //  self.SelectedDistricId = self.DistricId![index]
        }
    }
    
    // MARK: - URL Validation
    func isValidUrl(url: String) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
    // MARK: - save database
    func SaveData(Desc:String, Createdby:Date,File:Data,ContentType:String,Filename:String,url:String)  {
        DBManager.sharedInstance.saveDownloadData(Desc, ContentType, Createdby, File, Filename, url)
    }
    
    func AlertControler(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func CallDownloadUrl(Url:String,Desc:String,contentType:String) {
        let currentDate = NSDate()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        // get the date time String from the date object
        formatter.string(from: currentDate as Date)
        AF.download(Url)
            .downloadProgress { progress  in
                // print("Download Progress: \(progress.fractionCompleted * 100) %")
                self.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
                
        }
        .responseData { response in
            if let data = response.value {
                //  let image = UIImage(data: data)
                self.SaveData(Desc: Desc, Createdby: currentDate as Date, File: data, ContentType: contentType, Filename:("Image\(formatter.string(from: currentDate as Date))") , url:Url)
                self.progressView.progress = 0.0
                self.AlertControler(message:"Download successfully.")
                self.txtURL.text = nil
            }
        }
    }
    
    func CallDownloadFile(strURL:String){
        HUD.show(.progress)
        if let FileUrl = URL(string:strURL) {
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(FileUrl.lastPathComponent)
            print(destinationUrl)
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                self.AlertControler(message:"The file already exists.")
                HUD.hide()
                // if the file doesn't exist
            } else {
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: FileUrl) { location, response, error in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                        let currentDate = NSDate()
                        let formatter = DateFormatter()
                        formatter.timeStyle = .short
                        formatter.dateStyle = .short
                        // get the date time String from the date object
                        formatter.string(from: currentDate as Date)
                        let Data = NSData(bytes: [] as [UInt8], length: 2)
                        DispatchQueue.main.async {
                            if let url = URL(string: "\(destinationUrl)"){
                                self.SaveData(Desc: self.txtDescription.text, Createdby: currentDate as Date, File:Data as Data , ContentType: self.ContentType, Filename:url.lastPathComponent, url:"\(url)")
                                HUD.hide()
                                self.txtURL.text = nil
                                self.AlertControler(message:"Download successfully.")
                            }
                        }
                    } catch {
                        print(error)
                        HUD.hide()
                    }
                }.resume()
            }
        }
    }
}

extension DownloadVC:UITextFieldDelegate, UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count + (text.count - range.length) <= 100
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write your Description"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension URL {
    func mimeType() -> String {
        let pathExtension = self.pathExtension
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
}
