//
//  ViewController.swift
//  Assignment

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnDownload(_ sender:UIButton){
        let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
        let Vc = storyboard.instantiateViewController(withIdentifier:"DownloadVC") as! DownloadVC
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func btnView(_ sender:UIButton){
        let storyboard = UIStoryboard.init(name:"Main", bundle:Bundle.main)
        let Vc = storyboard.instantiateViewController(withIdentifier:"ViewVC") as! ViewVC
        self.navigationController?.pushViewController(Vc, animated: true)
    }
}

